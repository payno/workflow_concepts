import sys
import logging
import pytest
from esrf2dask import job
from taskgraphlib import taskgraphs
from taskgraphlib import assert_taskgraph_result

logging.getLogger("dask").setLevel(logging.DEBUG)
logging.getLogger("dask").addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger("esrf2dask").setLevel(logging.DEBUG)
logging.getLogger("esrf2dask").addHandler(logging.StreamHandler(sys.stdout))


@pytest.mark.parametrize("scheduler", [None, "multithreading", "multiprocessing"])
def test_job(tmpdir, scheduler):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    job(graph, varinfo=varinfo, scheduler=scheduler)
    assert_taskgraph_result(graph, expected, varinfo)
