import os
from taskgraphlib import taskgraphs
from esrftaskgraph import load_graph


def show_graph(taskgraph):
    import networkx
    from pprint import pprint
    import matplotlib.pyplot as plt

    pprint(taskgraph.dump())
    networkx.draw(taskgraph.graph, with_labels=True)
    plt.show()


def multiprocess_scheduler(taskgraph, varinfo):
    # tasks are distributed over processes
    import esrf2multiprocessing

    esrf2multiprocessing.job(taskgraph, varinfo=varinfo)


def singlethread_scheduler(taskgraph, varinfo):
    # runs in a single thread
    taskgraph.execute(varinfo)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Example of task graph execution.")
    parser.add_argument(
        "--root_uri", type=str, default="/tmp", help="Location of the results"
    )
    parser.add_argument("--plot", action="store_true", help="Plot+print the task graph")
    parser.add_argument(
        "--scheduler",
        type=str,
        default="singlethread",
        choices=["none", "singlethread", "multiprocess"],
        help="Task scheduler",
    )
    args = parser.parse_args()

    # Persistent Variable storage
    varinfo = {"root_uri": args.root_uri}
    os.makedirs(varinfo["root_uri"], exist_ok=True)

    # Load example task graph
    taskgraph, expected = taskgraphs.acyclic_graph1()
    taskgraph = load_graph(taskgraph)

    # Run the graph if requested
    if args.scheduler == "multiprocess":
        multiprocess_scheduler(taskgraph, varinfo)
    elif args.scheduler == "singlethread":
        singlethread_scheduler(taskgraph, varinfo)

    # Verify the expected result for each task
    tasks = dict()
    print(f"Task graph results {repr(taskgraph)}:")
    for node, value in expected.items():
        taskgraph.instantiate_task_static(node, tasks=tasks, varinfo=varinfo)
        task = tasks[node]
        assert task.done, str(task)
        assert task.output_values == value, str(task)
        print(f" task {repr(node)}: {task.outputs}")

    if args.plot:
        show_graph(taskgraph)
