"""Create json files of some example graphs
"""

import os
from tempfile import mkdtemp
from taskgraphlib import taskgraphs
from esrftaskgraph import load_graph

outdir = mkdtemp(prefix="example_graphs")

names = [
    "empty_graph",
    "acyclic_graph1",
    "acyclic_graph2",
    "cyclic_graph1",
    "cyclic_graph2",
]
for name in names:
    graph, expected = getattr(taskgraphs, name)()
    filename = os.path.join(outdir, name + ".json")
    load_graph(graph).dump(filename, indent=2)
    print(filename)
