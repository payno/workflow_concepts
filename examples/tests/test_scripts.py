import os
import sys
from glob import glob
import subprocess
import pytest

root_dir = os.path.dirname(os.path.dirname(__file__))
scripts = [os.path.basename(f) for f in glob(os.path.join(root_dir, "*.py"))]


@pytest.mark.parametrize("filename", scripts)
def test_scripts(filename, tmpdir):
    if filename in ("example_graphs.py",):
        pytest.skip("Has side effects")
    script = os.path.join(root_dir, filename)
    args = [sys.executable, script, "--root_uri", str(tmpdir)]
    subprocess.run(args).check_returncode()


def test_running_taskgraphs(tmpdir):
    script = os.path.join(root_dir, "running_taskgraphs.py")
    args = [sys.executable, script, "--root_uri", str(tmpdir)]
    with pytest.raises(subprocess.CalledProcessError):
        subprocess.run(args + ["--scheduler", "none"]).check_returncode()
    subprocess.run(args + ["--scheduler", "multiprocess"]).check_returncode()
    subprocess.run(args + ["--scheduler", "none"]).check_returncode()
