import os
from glob import glob
from posixpath import basename
import pytest
import testbook

root_dir = os.path.dirname(os.path.dirname(__file__))
notebooks = [os.path.basename(f) for f in glob(os.path.join(root_dir, "*.ipynb"))]


@pytest.mark.parametrize("filename", notebooks)
def test_notebooks(filename):
    filename = os.path.join(root_dir, filename)
    with testbook.testbook(filename, execute=True):
        pass
