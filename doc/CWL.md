# Common Workflow Language

https://www.commonwl.org/user_guide/01-introduction/index.html

CWL is a way to describe command line tools and connect them together to create workflows.

### Python example
https://github.com/NLeSC/scriptcwl/tree/master/scriptcwl/examples

The implementation of a task
```python
# add.py
import click
import json

@click.command()
@click.argument('x', type=int)
@click.argument('y', type=int)
def add(x, y):
    click.echo(json.dumps({'answer': x+y}))

if __name__ == '__main__':
    add()
```

The description of a task
```yaml
# add.cwl
cwlVersion: v1.0
class: CommandLineTool
baseCommand: ["python", "add.py"]

inputs:
  x:
    type: int
    inputBinding:
      position: 1
  y:
    type: int
    inputBinding:
      position: 2

stdout: cwl.output.json

outputs:
  answer:
    type: int
```

The description of a workflow instance
```yaml
# myworkflow.cwl
cwlVersion: v1.0
class: Workflow
inputs:
  num1: int
  num2: int
outputs:
  final_answer:
    type: int
    outputSource: task2/answer
steps:
  task1:
    run: add.cwl
    in:
      y: num2
      x: num1
    out:
    - answer
  task2:
    run: add.cwl
    in:
      y: num2
      x: task1/answer
    out:
    - answer
```
