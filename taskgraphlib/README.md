# taskgraph

Example of a project that defines graphs based on task libraries deriving from *esrftaskgraph*. Or rather it defines graph instances. A graph instance is a graph with fixed static inputs.
