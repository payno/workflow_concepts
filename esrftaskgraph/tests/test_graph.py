import pytest
from taskgraphlib import taskgraphs
from taskgraphlib import assert_taskgraph_result
from esrftaskgraph.graph import load_graph


def test_graph_cyclic():
    graph, _ = taskgraphs.empty_graph()
    graph = load_graph(graph)
    assert not graph.is_cyclic
    graph, _ = taskgraphs.acyclic_graph1()
    graph = load_graph(graph)
    assert not graph.is_cyclic
    graph, _ = taskgraphs.cyclic_graph1()
    graph = load_graph(graph)
    assert graph.is_cyclic


def test_acyclic_execution(tmpdir):
    # Naive sequential task scheduler
    g, expected = taskgraphs.acyclic_graph1()
    taskgraph = load_graph(g)
    varinfo = {"root_uri": str(tmpdir)}
    taskgraph.execute(varinfo=varinfo)
    assert_taskgraph_result(taskgraph, expected, varinfo)


def test_cyclic_execution(tmpdir):
    g, expected = taskgraphs.cyclic_graph1()
    taskgraph = load_graph(g)
    varinfo = {"root_uri": str(tmpdir)}
    with pytest.raises(RuntimeError):
        taskgraph.execute(varinfo=varinfo)


def test_start_nodes(tmpdir):
    g, expected = taskgraphs.acyclic_graph1()
    taskgraph = load_graph(g)
    assert set(taskgraph.start_nodes()) == {"task1", "task2"}

    g, expected = taskgraphs.acyclic_graph2()
    taskgraph = load_graph(g)
    assert set(taskgraph.start_nodes()) == {"task1"}

    g, expected = taskgraphs.cyclic_graph1()
    taskgraph = load_graph(g)
    assert set(taskgraph.start_nodes()) == {"task1"}

    g, expected = taskgraphs.cyclic_graph2()
    taskgraph = load_graph(g)
    assert set(taskgraph.start_nodes()) == {"task1"}


def test_end_nodes(tmpdir):
    g, expected = taskgraphs.acyclic_graph1()
    taskgraph = load_graph(g)
    assert set(taskgraph.end_nodes()) == {"task6"}
    assert set(taskgraph.end_nodes(optional=True)) == set()
    assert set(taskgraph.end_nodes(optional=False)) == {"task6"}

    g, expected = taskgraphs.acyclic_graph2()
    taskgraph = load_graph(g)
    assert set(taskgraph.end_nodes()) == {"task5", "task6"}
    assert set(taskgraph.end_nodes(optional=True)) == {"task6"}
    assert set(taskgraph.end_nodes(optional=False)) == {"task5"}

    g, expected = taskgraphs.cyclic_graph1()
    taskgraph = load_graph(g)
    assert set(taskgraph.end_nodes()) == {"task4", "task6", "task7"}
    assert set(taskgraph.end_nodes(optional=True)) == {"task4", "task6", "task7"}
    assert set(taskgraph.end_nodes(optional=False)) == set()

    g, expected = taskgraphs.cyclic_graph2()
    taskgraph = load_graph(g)
    assert set(taskgraph.end_nodes()) == {"task1", "task2", "task3"}
    assert set(taskgraph.end_nodes(optional=True)) == {"task1", "task2", "task3"}
    assert set(taskgraph.end_nodes(optional=False)) == set()
