# esrftaskgraph

Runtime and persistent representation of task graphs supported by the ESRF DAU.

The graph theory library *networkx* provides representations and API for task schedulers that need to analyze and distribute tasks from a *Graph*.

## Definition

The definition of an ESRF task graph is

* *Graph*: all *directed graphs* are supported
* *Task*: a graph node which represents an opaque unit of execution with named input arguments (required and optional) and named output arguments (only required)
* *Link*: a graph edge with a direction. It allows data to be passed between tasks and is either
    * *unconditional*: the downstream task can only be executed when the upstream task has finished successfully
    * *conditional*: the downstream task does not require the upstream task

### Attributes

The elements of an ESRF task graph can have following attributes

#### Task
* *id*: node identifier in the graph (this is NOT the task *instance* identifier)
* *inputs* (optional): a dictionary of defaults for named inputs (used when not provided by the output of an upstream task)
* The unit of execution is specified by specifying one of these attributes:
    * *class*: the full qualifier name of a task class
    * *method*: the full qualifier name of a function
    * *ppfmethod*: the full qualifier name of a pypushflow function (special input/output convention)
    * *script*: the full qualifier name of a python or shell script
    * *graph*: the representation of another graph (e.g. json file name)
* *inputs_complete* (optional): required for non-*class* tasks to indicate that the *inputs* are complete which is needed for example to determine whether a task is a start task in cyclic graphs.

#### Link
* *source*: the id of the source node 
* *target*: the id of the target node
* *arguments* (optional): a dictionary that maps output names to input names. If the input name is `None` the output name receives the complete output of the source.
* *all_arguments* (optional): setting this to `True` is equivalent to *arguments* being the identity mapping for all input names
* *conditions* (optional): a dictionary that maps output names to expected values
* *on_error* (optional): a special condition
* *links*: when either the source or the target node is a graph, this list of dictionaries specifies the links between the super-graph and the sub-graph. The dictionary keys are
  * *source*: the id of the source node in super- or sub-graph
  * *target*: the id of the target node in super- or sub-graph
  * *node_attributes* (optional): overwrite the node attributes of the target when the target is a graph

#### Graph
* *name* (optional): the name of the task graph

## Implementation

### Task

All tasks are described by a `Task` class.
* required input names: an exception is raised when these inputs are not provided in the graph definition (output from previous tasks or static input values)
* optional input names: no default values provided (need to be done in the `process` method)
* output names: can be connected to downstream input names

When not specifying the `Task` class directly, a class wrapper will be used:
* *method*: defined by a `Task` class with one required input argument ("method": full qualifier name of the method) and one output argument ("return_value")
* *ppfmethod*: same as *method* but it has one optional input "ppfdict" and one output "ppfdict". The output dictonary is the input dictionary updated by the method. The input dictionary is unpacked before passing to the method. The output dictionary is unpacked when checking conditions in links.
* *ppfport*: *ppfmethod* which is the identity mapping
* *script*: defined by a `Task` class with one required input argument ("method": full qualifier name of the method) and one output argument ("return_value")

### Hash links
The *TaskGraph* provides additional functionality in top of what *networkx* provides:
* A *Task* can have several named input and output variables.
* A *Task* has a universal hash which is the hash of the inputs with a *Task* nonce.
* An output *Variable* has a universal hash which is the hash of the *Task* with the variable name as nonce.
* An input *Variable* can be
    * static:
        * provided by the persistent *Graph* representation
        * universal hash of the data
    * dynamic:
        * provided by upstream *Tasks* at runtime
        * output *Variable* of the upstream task so it has a universal hash

The actual output data of a *Task* is never hashed. So we assume that if you provide a task with the same input, you will get the same output. Or at the very least it will not be executed again when succeeded once.

Hash linking of tasks serves the following purpose:
* Changing static input upstream in the graph will effectively create new tasks.
* The hashes provide a unique ID to create a *URI* for persistent storage.
* Tasks can be provided with universial hashes instead of the actual inputs.
* As data can be passed by passing hashes, serialization for distibuted task scheduling can be done efficiently (not much data to serialize) and no special serializer is required to serialize hashes.

Data management is currently only a proof-of-concept based on JSON files with the universal hashes as file names.