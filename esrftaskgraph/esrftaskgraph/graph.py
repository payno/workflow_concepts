import enum
import networkx
import json
from collections.abc import Mapping
from esrftaskgraph import inittask
from esrftaskgraph.utils import qualname


def load_graph(source=None, representation=None):
    if isinstance(source, TaskGraph):
        return source
    else:
        return TaskGraph(source=source, representation=representation)


def set_graph_defaults(graph_as_dict):
    graph_as_dict.setdefault("directed", True)
    graph_as_dict.setdefault("nodes", list())
    graph_as_dict.setdefault("links", list())


def node_has_links(graph, node_name):
    try:
        next(graph.successors(node_name))
    except StopIteration:
        try:
            next(graph.predecessors(node_name))
        except StopIteration:
            return False
    return True


def validate_node(graph, node_name, node_attrs, nosubgraphs=False):
    inittask.validate_task_executable(
        node_attrs, node_name=node_name, all=not nosubgraphs
    )
    # Isolated nodes do no harm so comment this
    # if len(graph.nodes) > 1 and not node_has_links(graph, node_name):
    #    raise ValueError(f"Node {repr(node_name)} has no links")


def validate_edge(graph, source, target):
    if source not in graph.nodes:
        raise ValueError(
            f"Source node {repr(source)} of link |{repr(source)} -> {repr(target)}| does not exist"
        )
    if target not in graph.nodes:
        raise ValueError(
            f"Target node {repr(target)} of link |{repr(source)} -> {repr(target)}| does not exist"
        )


def validate_nodes(graph, nosubgraphs=False):
    for node_name, node_attrs in graph.nodes.items():
        validate_node(graph, node_name, node_attrs, nosubgraphs=nosubgraphs)


def merge_graphs(graphs, name=None, rename_nodes=None):
    lst = list()
    if rename_nodes is None:
        rename_nodes = [True] * len(graphs)
    else:
        assert len(graphs) == len(rename_nodes)
    for g, rename in zip(graphs, rename_nodes):
        g = load_graph(g)
        gname = repr(g)
        g = g.graph
        if rename:
            mapping = {s: (gname, s) for s in g.nodes}
            g = networkx.relabel_nodes(g, mapping, copy=True)
        lst.append(g)
    ret = load_graph(networkx.compose_all(lst))
    if name:
        ret.graph.graph["name"] = name
    return ret


def dict_merge(destination, source, overwrite=False, _nodes=None):
    """Merge the source into the destination"""
    if _nodes is None:
        _nodes = tuple()
    for key, value in source.items():
        if key in destination:
            _nodes += (str(key),)
            if isinstance(destination[key], Mapping) and isinstance(value, Mapping):
                dict_merge(destination[key], value, overwrite=overwrite, _nodes=_nodes)
            elif value == destination[key]:
                continue
            elif overwrite:
                destination[key] = value
            else:
                raise ValueError("Conflict at " + ".".join(_nodes))
        else:
            destination[key] = value


def flatten_multigraph(graph):
    if not graph.is_multigraph():
        return graph
    newgraph = networkx.DiGraph(**graph.graph)

    edgeattrs = dict()
    for edge, attrs in graph.edges.items():
        key = edge[:2]
        mergedattrs = edgeattrs.setdefault(key, dict())
        dict_merge(mergedattrs, attrs)

    for name, attrs in graph.nodes.items():
        newgraph.add_node(name, **attrs)
    for (source, target), mergedattrs in edgeattrs.items():
        newgraph.add_edge(source, target, **mergedattrs)
    return newgraph


def get_subgraphs(graph):
    subgraphs = dict()
    for node_name, node_attrs in graph.nodes.items():
        name, value = inittask.task_executable_key(
            node_attrs, node_name=node_name, all=True
        )
        if name == "graph":
            g = load_graph(value)
            g.graph.graph["name"] = node_name
            subgraphs[node_name] = g
    return subgraphs


def _pop_subgraph_node_name(subgraph_name, link_attrs, source=True):
    if source:
        key = "source"
    else:
        key = "target"
    try:
        subgraph_node_name = link_attrs.pop(key)
    except KeyError:
        raise ValueError(
            f"The '{key}' attribute to specify a node in subgraph '{subgraph_name}' is missing"
        ) from None
    return subgraph_name, subgraph_node_name


def extract_subgraphs(graph, subgraphs):
    # Edges between supergraph and subgraphs
    edges = list()
    update_attrs = dict()
    for subgraph_name, subgraph in subgraphs.items():
        for source_name in graph.predecessors(subgraph_name):
            target_name = subgraph_name
            links = graph[source_name][target_name].get("links", list())
            for link_attrs in links:
                link_attrs = dict(link_attrs)

                if source_name in subgraphs:
                    source = _pop_subgraph_node_name(
                        source_name, link_attrs, source=True
                    )
                else:
                    link_attrs.pop("source", None)
                    source = source_name
                target = _pop_subgraph_node_name(target_name, link_attrs, source=False)

                update_attrs[target] = link_attrs.pop("node_attributes", dict())
                edges.append((source, target, link_attrs))

        for target_name in graph.successors(subgraph_name):
            source_name = subgraph_name
            links = graph[source_name][target_name].get("links", list())
            for link_attrs in links:
                link_attrs = dict(link_attrs)

                if target_name in subgraphs:
                    target = _pop_subgraph_node_name(
                        target_name, link_attrs, source=False
                    )
                else:
                    link_attrs.pop("target", None)
                    target = target_name
                source = _pop_subgraph_node_name(source_name, link_attrs, source=True)

                link_attrs.pop("node_attributes", None)
                edges.append((source, target, link_attrs))

    graph.remove_nodes_from(subgraphs.keys())
    return edges, update_attrs


def add_subgraph_links(graph, edges, update_attrs):
    # Output from extract_subgraphs
    for source, target, _ in edges:
        validate_edge(graph, source, target)
    graph.add_edges_from(edges)  # This adds missing nodes
    for node, attrs in update_attrs.items():
        node_attrs = graph.nodes[node]
        if attrs:
            dict_merge(node_attrs, attrs, overwrite=True)


class TaskGraph:
    """The API for graph analysis is provided by `networkx`.
    Any directed graph is supported (cyclic or acyclic).

    Loop over the dependencies of a task

    .. code-block:: python

        for source in taskgraph.predecessors(target):
            link_attrs = taskgraph.graph[source][target]

    Loop over the tasks dependent on a task

    .. code-block:: python

        for target in taskgraph.successors(source):
            link_attrs = taskgraph.graph[source][target]

    Instantiate a task

    .. code-block:: python

        task = taskgraph.instantiate_task(name, varinfo=varinfo, inputs=inputs)

    For acyclic graphs, sequential task execution can be done like this:

    .. code-block:: python

        taskgraph.execute()
    """

    GraphRepresentation = enum.Enum(
        "GraphRepresentation", "json_file json_dict json_string yaml"
    )

    def __init__(self, source=None, representation=None):
        self.load(source=source, representation=representation)

    def __repr__(self):
        return self.graph.graph.get("name", qualname(type(self)))

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError(other, type(other))
        return self.dump() == other.dump()

    def load(self, source=None, representation=None):
        """From persistent to runtime representation"""
        if representation is None:
            if isinstance(source, Mapping):
                representation = self.GraphRepresentation.json_dict
            elif isinstance(source, str):
                if source.endswith(".json"):
                    representation = self.GraphRepresentation.json_file
                else:
                    representation = self.GraphRepresentation.json_string
        if not source:
            graph = networkx.DiGraph()
        elif isinstance(source, networkx.Graph):
            graph = source
        elif isinstance(source, TaskGraph):
            graph = source.graph
        elif representation == self.GraphRepresentation.json_dict:
            set_graph_defaults(source)
            graph = networkx.readwrite.json_graph.node_link_graph(source)
        elif representation == self.GraphRepresentation.json_file:
            with open(source, mode="r") as f:
                source = json.load(f)
            set_graph_defaults(source)
            graph = networkx.readwrite.json_graph.node_link_graph(source)
        elif representation == self.GraphRepresentation.json_string:
            source = json.loads(source)
            set_graph_defaults(source)
            graph = networkx.readwrite.json_graph.node_link_graph(source)
        elif representation == self.GraphRepresentation.yaml:
            graph = networkx.readwrite.read_yaml(source)
        else:
            raise TypeError(representation, type(representation))

        if not networkx.is_directed(graph):
            raise TypeError(graph, type(graph))

        graph = flatten_multigraph(graph)

        subgraphs = get_subgraphs(graph)
        if subgraphs:
            # Extract
            edges, update_attrs = extract_subgraphs(graph, subgraphs)

            # Merged
            self.graph = graph
            graphs = [self] + list(subgraphs.values())
            rename_nodes = [False] + [True] * len(subgraphs)
            graph = merge_graphs(
                graphs, name=graph.graph.get("name"), rename_nodes=rename_nodes
            ).graph

            # Re-link
            add_subgraph_links(graph, edges, update_attrs)

        self.graph = graph

    def dump(self, destination=None, representation=None, **kw):
        """From runtime to persistent representation"""
        if representation is None:
            if isinstance(destination, str) and destination.endswith(".json"):
                representation = self.GraphRepresentation.json_file
            else:
                representation = self.GraphRepresentation.json_dict
        if representation == self.GraphRepresentation.json_dict:
            return networkx.readwrite.json_graph.node_link_data(self.graph)
        elif representation == self.GraphRepresentation.json_file:
            dictrepr = self.dump()
            with open(destination, mode="w") as f:
                json.dump(dictrepr, f, **kw)
            return destination
        elif representation == self.GraphRepresentation.json_string:
            dictrepr = self.dump()
            return json.dumps(dictrepr, **kw)
        elif representation == self.GraphRepresentation.yaml:
            return networkx.readwrite.write_yaml(self.graph, destination, **kw)
        else:
            raise TypeError(representation, type(representation))

    def serialize(self):
        return self.dump(representation=self.GraphRepresentation.json_string)

    @property
    def is_cyclic(self):
        return not networkx.is_directed_acyclic_graph(self.graph)

    @property
    def has_conditional_links(self):
        for attrs in self.graph.edges.values():
            if attrs.get("conditions") or attrs.get("on_error"):
                return True
        return False

    def instantiate_task(self, node_name, varinfo=None, inputs=None):
        """Named arguments are dynamic input and Variable config.
        Static input from the persistent representation are
        added internally.

        :param str node_name:
        :param dict or None tasks: keeps upstream tasks
        :param **inputs: dynamic inputs
        :returns Task:
        """
        # Dynamic input has priority over static input
        nodeattrs = self.graph.nodes[node_name]
        return inittask.instantiate_task(
            nodeattrs, node_name=node_name, varinfo=varinfo, inputs=inputs
        )

    def instantiate_task_static(self, node_name, tasks=None, varinfo=None, inputs=None):
        """Instantiate destination task while no or partial access to the dynamic
        inputs or their identifiers. Side effect: `tasks` will contain all predecessors.

        Remark: Only works for DAGs.

        :param str node_name:
        :param dict or None tasks: keeps upstream tasks
        :param dict or None inputs: optional dynamic inputs
        :returns Task:
        """
        if self.is_cyclic:
            raise RuntimeError(f"{self} is cyclic")
        if tasks is None:
            tasks = dict()
        dynamic_inputs = dict()
        for inputnode in self.predecessors(node_name):
            inputtask = tasks.get(inputnode, None)
            if inputtask is None:
                inputtask = self.instantiate_task_static(
                    inputnode, tasks=tasks, varinfo=varinfo
                )
            link_attrs = self.graph[inputnode][node_name]
            all_arguments = link_attrs.get("all_arguments", False)
            if all_arguments:
                arguments = {s: s for s in inputtask.output_variables}
                for from_arg in inputtask.output_variables:
                    to_arg = from_arg
                    dynamic_inputs[to_arg] = inputtask.output_variables[from_arg]
            arguments = link_attrs.get("arguments", dict())
            for to_arg, from_arg in arguments.items():
                if from_arg:
                    dynamic_inputs[to_arg] = inputtask.output_variables[from_arg]
                else:
                    dynamic_inputs[to_arg] = inputtask.output_variables
        if inputs:
            dynamic_inputs.update(inputs)
        task = self.instantiate_task(node_name, varinfo=varinfo, inputs=dynamic_inputs)
        tasks[node_name] = task
        return task

    def successors(self, node_name, **include_filter):
        yield from self._iter_downstream_nodes(
            node_name, recursive=False, **include_filter
        )

    def descendants(self, node_name, **include_filter):
        yield from self._iter_downstream_nodes(
            node_name, recursive=True, **include_filter
        )

    def predecessors(self, node_name, **include_filter):
        yield from self._iter_upstream_nodes(
            node_name, recursive=False, **include_filter
        )

    def ancestors(self, node_name, **include_filter):
        yield from self._iter_upstream_nodes(
            node_name, recursive=True, **include_filter
        )

    def has_successors(self, node_name, **include_filter):
        try:
            next(self.successors(node_name, **include_filter))
            return True
        except StopIteration:
            return False

    def has_descendants(self, node_name, **include_filter):
        try:
            next(self.descendants(node_name, **include_filter))
            return True
        except StopIteration:
            return False

    def has_predecessors(self, node_name, **include_filter):
        try:
            next(self.predecessors(node_name, **include_filter))
            return True
        except StopIteration:
            return False

    def has_ancestors(self, node_name, **include_filter):
        try:
            next(self.ancestors(node_name, **include_filter))
            return True
        except StopIteration:
            return False

    def _iter_downstream_nodes(
        self,
        source_name,
        recursive=False,
        _visited=None,
        **include_filter,
    ):
        if recursive:
            if _visited is None:
                _visited = list()
            elif source_name in _visited:
                return
            _visited.append(source_name)
        for target_name in self.graph.successors(source_name):
            if self._filter_node(target_name, **include_filter) and self._filter_link(
                source_name, target_name, **include_filter
            ):
                yield target_name
            if recursive:
                yield from self._iter_downstream_nodes(
                    target_name,
                    recursive=True,
                    _visited=_visited,
                    **include_filter,
                )

    def _iter_upstream_nodes(
        self,
        target_name,
        recursive=False,
        _visited=None,
        **include_filter,
    ):
        if recursive:
            if _visited is None:
                _visited = list()
            elif target_name in _visited:
                return
            _visited.append(target_name)
        for source_name in self.graph.predecessors(target_name):
            if self._filter_node(source_name, **include_filter) and self._filter_link(
                source_name, target_name, **include_filter
            ):
                yield source_name
            if recursive:
                yield from self._iter_upstream_nodes(
                    source_name,
                    recursive=True,
                    _visited=_visited,
                    **include_filter,
                )

    def _filter_node(
        self,
        node_name,
        node_filter=None,
        has_predecessors=None,
        has_successors=None,
        **linkfilter,
    ):
        node_attrs = self.graph.nodes[node_name]
        if callable(node_filter):
            if not node_filter(node_attrs):
                return False
        if has_predecessors is not None:
            if self.has_predecessors(node_name) != has_predecessors:
                return False
        if has_successors is not None:
            if self.has_successors(node_name) != has_successors:
                return False
        return True

    def _filter_link(
        self,
        source_name,
        target_name,
        link_filter=None,
        on_error=None,
        conditional=None,
        **nodefilter,
    ):
        link_attrs = self.graph[source_name][target_name]
        if callable(link_filter):
            if not link_filter(link_attrs):
                return False
        if on_error is not None:
            if bool(link_attrs.get("on_error", False)) != on_error:
                return False
        if conditional is not None:
            if bool(link_attrs.get("conditions", False)) != conditional:
                return False
        return True

    def has_required_inputs(self, node_name):
        """Returns True when the static inputs cover all required inputs."""
        node_attrs = self.graph.nodes[node_name]
        inputs_complete = node_attrs.get("inputs_complete", None)
        if isinstance(inputs_complete, bool):
            return inputs_complete
        taskclass = inittask.get_task_class(node_attrs, node_name=node_name)
        static_inputs = node_attrs.get("inputs", dict())
        return not (set(taskclass.required_input_names()) - set(static_inputs.keys()))

    def start_nodes(self):
        for node_name in self.graph.nodes:
            if self.is_start_node(node_name):
                yield node_name

    def end_nodes(self, optional=None):
        for node_name in self.graph.nodes:
            if self.is_end_node(node_name):
                if optional is not None:
                    if self.in_optional_branch(node_name) != optional:
                        continue
                yield node_name

    def is_start_node(self, node_name):
        if not self.has_predecessors(node_name):
            # A node without predecessors is a start node
            return True
        if self.has_predecessors(node_name, conditional=False):
            # A node with unconditional predecessors cannot be a start node
            return False
        if self.has_ancestors(node_name, has_predecessors=False):
            # A node with a "pure" start node in its ancestors
            # cannot be a start node
            return False
        # A node with only conditional predecessors is a start node
        # when all required inputs are statically defined
        return self.has_required_inputs(node_name)

    def is_end_node(self, node_name):
        # A node without unconditional successors is an end node
        # TODO: default condition is not handled yet ("other" in pypushflow)
        return not self.has_successors(node_name, conditional=False, on_error=False)

    def in_optional_branch(self, node_name):
        # TODO: default condition is not handled yet ("other" in pypushflow)
        def link_filter(link_attrs):
            return link_attrs.get("on_error", False) or link_attrs.get(
                "conditions", False
            )

        return self.has_ancestors(node_name, link_filter=link_filter)

    def topological_sort(self):
        """Sort node names for sequential instantiation+execution of DAGs"""
        if self.is_cyclic:
            raise RuntimeError("Sorting nodes is not possible for cyclic graphs")
        yield from networkx.topological_sort(self.graph)

    def execute(self, varinfo=None):
        """Sequential execution of DAGs"""
        tasks = dict()
        if self.has_conditional_links:
            raise RuntimeError("Cannot execute graphs with conditional links")
        for node in self.topological_sort():
            task = self.instantiate_task_static(node, tasks=tasks, varinfo=varinfo)
            task.execute()
        return tasks
