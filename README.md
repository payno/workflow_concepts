# workflow_concepts

This project is meant to find the optimal workflow eco-system to be maintained by the ESRF DAU.

## Getting started

Developers install of all projects in this workflow eco-system
```bash
./devinstall.sh
```

Run the tests

```bash
pytest
```

Jupyter notebook examples

```bash
python -m jupyter notebook examples/
```

Run a script example

```bash
python examples/running_taskgraphs.py --plot
```

## Eco-system

### Core projects

Common runtime and persistent representation of tasks and task graphs
* *esrftaskgraph*: based on *networkx* with task and data management (currently a proof-of-concept with JSON files)

Bindings
* *esrf2pypushflow*: multiprocessing task scheduler
* *esrf2dask*: local and centralized task schedulers (DAGs only)
* *esrf2orange3*: graph design and execution GUI (DAGs only)

Bindings we will probably not use
* *esrf2luigi*: local and centralized task scheduler (DAGs only)
* *esrf2multiprocessing*: multiprocessing task scheduler (DAGs only)
* *esrf2paradag*: multitheading task scheduler (DAGs only)

### Technique specific projects
Only dependent on *esrftaskgraph*
* *tasklib*: example library of task implementations based on the *esrftaskgraph* abstraction.
* *graphlib*: example library with graphs of *tasklib* tasks. These are beamline/proposal specific graphs.

Using the bindings
* *orange3widgetlib*: example library of Orange widgets for *tasklib* tasks.

Some task libraries could become part of the core projects (PCA, FFT, SIFT, ...).

## Nomenclature

### Graphs
* *Graph* (a.k.a. Task Graph, Workflow): list of Tasks and Links
* *Graph instance*: a *Graph* with fixed static inputs
* *Task* (a.k.a. Node, Process): node in a task *Graph*
* *Task instance*: a *Task* with fixed inputs
* *Link* (a.k.a Edge): edge of a *Graph*
* *Variable*: task input and output
* *Pipeline*: a linear task *Graph*

#### Types of graphs:
* Directed graphs: edges have orientations (uni/bi-directional)
  * Simple directed graphs: no self-connecting nodes
    * Oriented graphs: no bidirectional edges
      * Directed acyclic graphs (DAGs): no directed cycles

We aim at supporting *Directed graphs*.

#### Graph design:
Apart from persistent representations (json, yaml, ...), graphs can be designed with GUIs. Often these GUIs also allow starting and monitoring a task scheduler.

Examples: Orange3, Rabix, ...

### Schedulers
Scheduler (a.k.a. Engine, Executor)

#### Task Scheduler
Execute/distribute tasks from a *graph instance*

Examples: Luigi, Airflow, dask, Orange3, pypushflow, Rabix, ...

#### Job Scheduler
Execute/distribute jobs

A *job* is one execution of a *graph instance* to completion, error or interrupt. A job could however be anything else, not related to task graphs.

Examples: Zocalo