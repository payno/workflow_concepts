import luigi
from esrftaskgraph import load_graph


class EsrfTarget(luigi.Target):
    def __init__(self, esrftask):
        self._esrftask = esrftask
        super().__init__()

    def exists(self):
        return self._esrftask.done


class TaskRunner(luigi.Task):
    node = luigi.Parameter()
    seresrfgraph = luigi.Parameter()
    varinfo = luigi.DictParameter()

    def __init__(self, *args, **kw):
        self._esrfgraph = None
        self._esrftask = None
        super().__init__(*args, **kw)

    def __repr__(self):
        return self.node

    def requires(self):
        kw = {"seresrfgraph": self.seresrfgraph, "varinfo": self.varinfo}
        return {
            predecessor: TaskRunner(node=predecessor, **kw)
            for predecessor in self.esrfgraph.predecessors(self.node)
        }

    @property
    def esrfgraph(self):
        if self._esrfgraph is None:
            self._esrfgraph = load_graph(self.seresrfgraph)
        return self._esrfgraph

    @property
    def esrftask(self):
        if self._esrftask is None:
            self._esrftask = self.esrfgraph.instantiate_task_static(
                self.node, varinfo=self.varinfo
            )
        return self._esrftask

    def output(self):
        return EsrfTarget(self.esrftask)

    def run(self):
        self.esrftask.execute()
        return dict(self.esrftask.output_uhashes)


def job(graph, representation=None, varinfo=None, scheduler=None):
    esrfgraph = load_graph(source=graph, representation=representation)
    if esrfgraph.is_cyclic:
        raise RuntimeError("luigi can only execute DAGs")
    if esrfgraph.has_conditional_links:
        raise RuntimeError("luigi cannot handle conditional links")

    # Schedulers has pull logic
    tasks = list()
    taskkw = {"seresrfgraph": esrfgraph.serialize(), "varinfo": varinfo}
    for node in esrfgraph.graph.nodes:
        if len(list(esrfgraph.graph.successors(node))) == 0:
            task = TaskRunner(node=node, **taskkw)
            tasks.append(task)

    # Run scheduler
    if scheduler is None:
        scheduler = dict()
    scheduler.setdefault("local_scheduler", True)
    scheduler.setdefault("workers", 10)
    scheduler.setdefault("log_level", "INFO")
    return luigi.build(tasks, **scheduler)
