import sys
import logging
from esrf2luigi import job
from taskgraphlib import taskgraphs
from taskgraphlib import assert_taskgraph_result

logging.getLogger("esrf2luigi").setLevel(logging.DEBUG)
logging.getLogger("esrf2luigi").addHandler(logging.StreamHandler(sys.stdout))


def test_job(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    scheduler = {"log_level": "DEBUG"}
    job(graph, varinfo=varinfo, scheduler=scheduler)
    assert_taskgraph_result(graph, expected, varinfo)


def test_job_centralized(tmpdir, luigid):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    scheduler = {
        "local_scheduler": False,
        "log_level": "DEBUG",
        "scheduler_port": luigid,
    }
    job(
        graph,
        varinfo=varinfo,
        scheduler=scheduler,
    )
    assert_taskgraph_result(graph, expected, varinfo)
