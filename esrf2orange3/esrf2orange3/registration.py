import pkgutil
import importlib
import pkg_resources
import logging
from esrf2orange3 import setuptools

from orangecanvas.registry.base import WidgetRegistry

# from orangecanvas.registry.discovery import WidgetDiscovery
from orangewidget.workflow.discovery import WidgetDiscovery

logger = logging.getLogger(__name__)


def add_entry_points(distroname, entry_points):
    """Add entry points to a package distribution

    :param dict entry_points: mapping of "groupname" to a list of entry points
                              ["ep1 = destination1", "ep1 = destination2", ...]
    """
    try:
        dist = pkg_resources.get_distribution(distroname)
    except Exception:
        print("Existing distributions:")
        print(list(pkg_resources.working_set.by_key.keys()))
        raise
    entry_map = dist.get_entry_map()
    for group, lst in entry_points.items():
        group_map = entry_map.setdefault(group, dict())
        for entry_point in lst:
            ep = pkg_resources.EntryPoint.parse(entry_point, dist=dist)
            if ep.name in group_map:
                raise ValueError(
                    f"Entry point {repr(ep.name)} already exists in group {repr(group)} of distribution {repr(distroname)}"
                )
            group_map[ep.name] = ep


def get_subpackages(package):
    for pkginfo in pkgutil.walk_packages(package.__path__, package.__name__ + "."):
        yield importlib.import_module(pkginfo.name)


def register_addon_package(package, distroname=None):
    """An package which has been installed as a normal package, i.e.
    not part of the orangecontrib namespace.

    :param package:
    :param str distroname: the name given too setuptools.setup
    """
    entry_points = dict()
    packages = get_subpackages(package)
    setuptools.update_entry_points(packages, entry_points)
    if not distroname:
        distroname = package.__name__
    add_entry_points(distroname, entry_points)


def widget_discovery(discovery, distroname, subpackages):
    dist = pkg_resources.get_distribution(distroname)
    for pkg in subpackages:
        discovery.process_category_package(pkg, distribution=dist)


def iter_entry_points(group):
    for ep in pkg_resources.iter_entry_points(group):
        if ep.dist.project_name.lower() != "orange3":
            yield ep


def get_owwidget_descriptions():
    reg = WidgetRegistry()
    disc = WidgetDiscovery(reg)
    disc.run(iter_entry_points(setuptools.WIDGET_GROUP))
    return reg.widgets()
