import os
import sys
import tempfile

from Orange.canvas.__main__ import main as launchcanvas

from Orange.widgets.widget import OWWidget, WidgetMetaClass
from Orange.widgets.widget import Input, Output
from Orange.widgets.settings import Setting

from esrftaskgraph import load_graph
from esrftaskgraph import Variable
from esrftaskgraph import TaskInputError
from esrf2orange3 import owsconvert


__all__ = ["job", "OWESRFWidget"]


def input_setter(name):
    def setter(self, var):
        self.set_input(name, var)

    return setter


def prepare_owesrfwidgetclass(
    attr, esrftaskclass=None, inputnamemap=None, outputnamemap=None
):
    """This needs to be called before signal and setting parsing"""
    if esrftaskclass is None:
        return

    class Inputs:
        pass

    class Outputs:
        pass

    attr["esrftaskclass"] = esrftaskclass
    attr["Inputs"] = Inputs
    attr["Outputs"] = Outputs
    attr["static_input"] = Setting({name: None for name in esrftaskclass.input_names()})
    attr["varinfo"] = Setting({"root_uri": "/tmp"})
    attr["static_input"].schema_only = True
    attr["varinfo"].schema_only = True

    if inputnamemap is None:
        inputnamemap = inputnamemap
    if outputnamemap is None:
        outputnamemap = outputnamemap

    for name in esrftaskclass.input_names():
        inpt = Input(inputnamemap.get(name, name), Variable)
        setattr(Inputs, name, inpt)
        funcname = "_setter_" + name
        method = input_setter(name)
        method.__name__ = funcname
        attr[funcname] = inpt(method)

    for name in esrftaskclass.output_names():
        output = Output(outputnamemap.get(name, name), Variable)
        setattr(Outputs, name, output)


class ESRFWidgetMetaClass(WidgetMetaClass):
    def __new__(
        metacls,
        name,
        bases,
        attr,
        esrftaskclass=None,
        inputnamemap=None,
        outputnamemap=None,
        **kw
    ):
        prepare_owesrfwidgetclass(
            attr,
            esrftaskclass=esrftaskclass,
            inputnamemap=inputnamemap,
            outputnamemap=outputnamemap,
        )
        return super().__new__(metacls, name, bases, attr, **kw)


class OWESRFWidget(OWWidget, metaclass=ESRFWidgetMetaClass, openclass=True):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.dynamic_input_variables = dict()
        self.output_variables = dict()

    @classmethod
    def input_names(cls):
        return cls.esrftaskclass.input_names()

    @classmethod
    def output_names(cls):
        return cls.esrftaskclass.output_names()

    @property
    def input_variables(self):
        variables = dict()
        for name in self.input_names():
            var = self.dynamic_input_variables.get(name)
            if var is None or var.value is None:
                value = self.static_input.get(name)
                var = Variable(value=value)
            variables[name] = var
        return variables

    @property
    def input_values(self):
        return {k: v.value for k, v in self.input_variables.items()}

    @property
    def dynamic_input_values(self):
        return {k: v.value for k, v in self.dynamic_input_variables.items()}

    @property
    def output_values(self):
        return {k: v.value for k, v in self.output_variables.items()}

    def set_input(self, name, var):
        if var is None:
            self.dynamic_input_variables.pop(name, None)
        else:
            if not isinstance(var, Variable):
                raise TypeError(var, Variable)
            self.dynamic_input_variables[name] = var

    def trigger_downstream(self):
        for name, var in self.output_variables.items():
            channel = getattr(self.Outputs, name)
            if var.value is None:
                channel.send(None)  # or invalidate?
            else:
                channel.send(var)

    def clear_downstream(self):
        for name in self.output_variables:
            channel = getattr(self.Outputs, name)
            channel.send(None)  # or invalidate?

    def run(self):
        task = self.esrftaskclass(**self.input_variables, varinfo=self.varinfo)
        try:
            task.execute()
        except TaskInputError:
            self.clear_downstream()
            return
        except Exception:
            self.clear_downstream()
            raise
        self.output_variables = task.output_variables
        self.trigger_downstream()

    def changeStaticInput(self):
        self.handleNewSignals()

    def handleNewSignals(self):
        self.run()


def job(graph, representation=None, varinfo=None):
    esrfgraph = load_graph(source=graph, representation=representation)
    if esrfgraph.is_cyclic:
        raise RuntimeError("Orange can only execute DAGs")
    if esrfgraph.has_conditional_links:
        raise RuntimeError("Orange cannot handle conditional links")

    # We do not have a mapping between OWS and the runtime representation.
    # So map to a (temporary) persistent representation first.
    with tempfile.TemporaryDirectory() as tmpdirname:
        filename = os.path.join(tmpdirname, "esrftaskgraph.ows")
        owsconvert.esrf_to_ows(esrfgraph, filename, varinfo=varinfo)
        argv = [sys.argv[0], filename]
        launchcanvas(argv=argv)
