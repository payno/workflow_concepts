from setuptools import setup, find_packages

setup(
    name="esrf2orange3",
    packages=find_packages(),
    entry_points={
        "console_scripts": ("orange-canvas-esrf = esrf2orange3.canvas.__main__:main",)
    },
)
