# esrf2orange3

*esrftaskgraph* binding for the task scheduler and graph design GUI of *Orange3*.

Orange3 addon libraries can register their widgets with *orangecontrib* upon distribution. Alternatively an addon can be installed like any other python package and the registration can be done before launching the canvas with `register_addon_package`. Such an addon cannot be auto-discovered by Orange3 from Pypi.
