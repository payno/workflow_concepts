from importlib import resources
from esrf2orange3 import owsconvert
from esrftaskgraph import load_graph
import taskgraphlib
from orangecontrib.evaluate.submodule import tutorials


def test_ows_to_esrf(tmpdir):
    with resources.path(tutorials, "sumtask_tutorial2.ows") as filename:
        esrfgraph = owsconvert.ows_to_esrf(str(filename))

    destination = str(tmpdir / "esrfgraph.ows")
    owsconvert.esrf_to_ows(esrfgraph, destination)
    esrfgraph2 = owsconvert.ows_to_esrf(destination)
    assert esrfgraph == esrfgraph2


def test_esrf_to_ows(tmpdir):
    graph, _ = taskgraphlib.acyclic_graph1()
    esrfgraph = load_graph(graph)

    destination = str(tmpdir / "esrfgraph2.ows")
    owsconvert.esrf_to_ows(esrfgraph, destination)

    esrfgraph2 = owsconvert.ows_to_esrf(destination)
    assert esrfgraph.dump() == esrfgraph2.dump()
