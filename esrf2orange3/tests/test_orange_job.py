import sys
import logging
from esrf2orange3 import job
from taskgraphlib import taskgraphs
from taskgraphlib import assert_taskgraph_result

logging.getLogger("orange").setLevel(logging.DEBUG)
logging.getLogger("orange").addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger("esrf2orange3").setLevel(logging.DEBUG)
logging.getLogger("esrf2orange3").addHandler(logging.StreamHandler(sys.stdout))


def test_job(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    # TODO: hashes are different due to static input
    # job(graph, varinfo=varinfo)
    # assert_taskgraph_result(graph, expected, varinfo)
