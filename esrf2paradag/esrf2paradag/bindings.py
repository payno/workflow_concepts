from paradag import DAG
from paradag import dag_run
from paradag import MultiThreadProcessor
from paradag import CallableExecutor
from paradag import FullSelector
from esrftaskgraph import load_graph


def convert_graph(esrfgraph, varinfo):
    pdgraph = DAG()
    tasks = dict()
    for node in esrfgraph.graph.nodes:
        task = esrfgraph.instantiate_task_static(node, tasks=tasks, varinfo=varinfo)
        pdgraph.add_vertex(task.execute)
    for node in esrfgraph.graph.nodes:
        task = tasks[node]
        for upstream in esrfgraph.predecessors(node):
            pdgraph.add_edge(tasks[upstream].execute, task.execute)
    return pdgraph


def job(graph, representation=None, varinfo=None):
    esrfgraph = load_graph(source=graph, representation=representation)
    if esrfgraph.is_cyclic:
        raise RuntimeError("paradag can only execute DAGs")
    if esrfgraph.has_conditional_links:
        raise RuntimeError("paradag cannot handle conditional links")

    pdgraph = convert_graph(esrfgraph, varinfo)
    dag_run(
        pdgraph,
        selector=FullSelector(),
        processor=MultiThreadProcessor(),
        executor=CallableExecutor(),
    )
