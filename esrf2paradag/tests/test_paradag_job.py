import sys
import logging
from esrf2paradag import job
from taskgraphlib import taskgraphs
from taskgraphlib import assert_taskgraph_result

logging.getLogger("paradag").setLevel(logging.DEBUG)
logging.getLogger("paradag").addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger("esrf2paradag").setLevel(logging.DEBUG)
logging.getLogger("esrf2paradag").addHandler(logging.StreamHandler(sys.stdout))


def test_job(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    job(graph, varinfo=varinfo)
    assert_taskgraph_result(graph, expected, varinfo)
