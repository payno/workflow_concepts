import sys
import logging
from esrf2multiprocessing import job
from taskgraphlib import taskgraphs
from taskgraphlib import assert_taskgraph_result

logging.getLogger("multiprocessing_dag").setLevel(logging.DEBUG)
logging.getLogger("multiprocessing_dag").addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger("esrf2multiprocessing").setLevel(logging.DEBUG)
logging.getLogger("esrf2multiprocessing").addHandler(logging.StreamHandler(sys.stdout))


def test_job(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    job(graph, varinfo=varinfo)
    assert_taskgraph_result(graph, expected, varinfo)
