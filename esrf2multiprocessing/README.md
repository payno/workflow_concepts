# esrf2multiprocessing

*esrftaskgraph* binding for the task scheduler of *multiprocessing-dag*.

*multiprocessing-dag* is basically a task scheduler for DAGs based on python's multiprocessing.
