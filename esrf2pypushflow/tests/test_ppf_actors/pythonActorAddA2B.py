import time


def run(a=None, **kwargs):

    time.sleep(1)
    if a is None:
        raise RuntimeError("Missing argument 'value'!")
    b = a + 1
    return {"b": b}
