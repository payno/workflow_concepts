import time


def run(b=None, **kwargs):

    time.sleep(1)
    if b is None:
        raise RuntimeError("Missing argument 'value'!")
    c = b + 1
    return {"c": c}
