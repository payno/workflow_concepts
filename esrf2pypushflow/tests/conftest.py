import sys
import logging
import pytest

# loggers = [logging.getLogger("pypushflow"), logging.getLogger("esrf2pypushflow")]
loggers = [logging.getLogger("esrf2pypushflow")]


@pytest.fixture(scope="session")
def ppf_logging():
    stdouthandler = logging.StreamHandler(sys.stdout)
    levels = []
    for logger in loggers:
        levels.append(logger.getEffectiveLevel())
        logger.setLevel(logging.DEBUG)
        logger.addHandler(stdouthandler)

    yield

    for level, logger in zip(levels, loggers):
        logger.setLevel(level)
        logger.removeHandler(stdouthandler)
