def CommonPrepareExperiment():
    nodes = [
        {"id": "In", "ppfport": "input"},
        {
            "id": "Init Workflow",
            "ppfmethod": "mx.src.init_workflow.run",
        },
        {
            "id": "Default parameters",
            "ppfmethod": "mx.src.common_default_parameters.run",
        },
        {
            "id": "Read motor positions",
            "ppfmethod": "mx.src.read_motor_positions.run",
        },
        {"id": "Out", "ppfport": "output"},
    ]
    links = [
        {"source": "In", "target": "Init Workflow", "all_arguments": True},
        {
            "source": "Init Workflow",
            "target": "Read motor positions",
            "all_arguments": True,
        },
        {
            "source": "Read motor positions",
            "target": "Default parameters",
            "all_arguments": True,
        },
        {"source": "Default parameters", "target": "Out", "all_arguments": True},
    ]

    graph = {
        "directed": True,
        "graph": {"name": "Common Prepare Experiment"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    return graph
