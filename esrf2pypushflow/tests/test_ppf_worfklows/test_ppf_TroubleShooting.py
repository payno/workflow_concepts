import os
import sys
import unittest

from esrf2pypushflow import job
from tests.test_ppf_worfklows.TroubleShooting import TroubleShooting

# Python path to BES mx modules
sys.path.insert(
    0, "/mnt/multipath-shares/sware/exp/pxsoft/bes/vgit/linux-x86_64/id30a2/bes/bes"
)
sys.path.insert(
    0, "/mnt/multipath-shares/sware/exp/pxsoft/bes/vgit/linux-x86_64/id30a2/edna2"
)


# This test will only work for users 'opid30' or 'svensson' due to file
# system permissions. Also note that this workflow needs a working
# MXCuBE instance to succeed
@unittest.skipIf(
    not os.environ["USER"] in ["opid30", "svensson"],
    "Only users 'opid30'and 'svensson' can run this test",
)
def test_TroubleShooting(tmpdir):
    # Set up pypushflow communication with mongodb
    os.environ[
        "PYPUSHFLOW_MONGOURL"
    ] = "mongodb://pybes:pybes@pybesdb1.esrf.fr:27017/pybes"
    inData = {
        "beamline": "id30a2",
        "prefix": "t1",
        "run_number": 1,
        "directory": "/data/id30a2/inhouse/opid30a2/20210421/RAW_DATA/t1",
    }
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = TroubleShooting(inData=inData)
    job(graph, varinfo=varinfo)
