import sys
import logging
from pypushflow.Workflow import Workflow
from pypushflow.StopActor import StopActor
from pypushflow.StartActor import StartActor
from pypushflow.PythonActor import PythonActor
from pypushflow.JoinActor import JoinActor
from pypushflow.RouterActor import RouterActor
from pypushflow.ErrorHandler import ErrorHandler
from pypushflow.AbstractActor import AbstractActor

from esrftaskgraph import load_graph
from esrf2pypushflow import ppfrunscript
from esrftaskgraph import Variable
from esrftaskgraph.inittask import task_executable


# Scheme: task graph
# Workflow: instance of a task graph
# Actor: task scheduler mechanism (trigger downstream taskactors)
# PythonActor: trigger execution of an method (full qualifier name)
#              in subprocess (python's multiprocessing)


logger = logging.getLogger(__name__)


class EsrfPythonActor(PythonActor):
    def __init__(self, node_name, node_attrs, **kw):
        self.node_name = node_name
        self.node_attrs = node_attrs
        kw["name"] = str(node_name)
        super().__init__(**kw)

    def trigger(self, inData):
        """
        :param dict inData: output from the previous task
        """
        infokey = ppfrunscript.INFOKEY
        inData[infokey] = dict(inData[infokey])
        inData[infokey]["node_name"] = self.node_name
        inData[infokey]["node_attrs"] = self.node_attrs
        return super().trigger(inData)


class InputMergeActor(JoinActor):
    """TODO: copy&paste from JoinActor with this modification:
    >= instead of == for cyclic tasks
    """

    def trigger(self, inData):
        self.setStarted()
        self.setFinished()
        self.listInData.append(inData)
        if len(self.listInData) >= self.numberOfThreads:
            newInData = {}
            for data in self.listInData:
                newInData.update(data)
            for actor in self.listDownStreamActor:
                actor.trigger(newInData)


class DecodeRouterActor(RouterActor):
    """TODO: copy&paste from RouterActor with this modification:
    >= instead of == for cyclic tasks
    """

    OTHERVALUE = "other"

    def __init__(self, parent, is_ppfmethod=False, **kw):
        self.is_ppfmethod = is_ppfmethod
        super().__init__(parent, **kw)

    def _extractPersistentValue(self, inData):
        if self.is_ppfmethod:
            uhash = inData["ppfdict"]
        else:
            if self.itemName in inData:
                uhash = inData[self.itemName]
            else:
                return self.OTHERVALUE
        varinfo = inData[ppfrunscript.INFOKEY]["varinfo"]
        value = Variable(uhash=uhash, varinfo=varinfo).value
        if self.is_ppfmethod:
            if self.itemName in value:
                value = value[self.itemName]
            else:
                return self.OTHERVALUE
        if value in self.dictValues:
            return value
        else:
            return self.OTHERVALUE

    def _extractValue(self, inData):
        if self.is_ppfmethod:
            inData = inData["ppfdict"]
        if self.itemName in inData:
            value = inData[self.itemName]
            if value in self.dictValues:
                return value
        return self.OTHERVALUE

    def trigger(self, inData):
        self.setStarted()
        self.setFinished()
        varinfo = inData[ppfrunscript.INFOKEY]["varinfo"]
        if varinfo.get("root_uri", None):
            value = self._extractPersistentValue(inData)
        else:
            value = self._extractValue(inData)
        actors = self.dictValues.get(value, list())
        for actor in actors:
            actor.trigger(inData)


class NameMapperActor(AbstractActor):
    """Maps output names to downstream input names"""

    def __init__(
        self,
        namemap=None,
        mapall=False,
        name="Name mapper",
        trigger_on_error=False,
        **kw,
    ):
        super().__init__(name=name, **kw)
        self.namemap = namemap
        self.mapall = mapall
        self.trigger_on_error = trigger_on_error

    def trigger(self, inData):
        is_error = "WorkflowException" in inData
        if is_error and not self.trigger_on_error:
            return
        try:
            newInData = dict()
            if not is_error:
                # Map output names of this task to input
                # names of the downstream task
                if self.mapall:
                    newInData.update(inData)
                for input_name, output_name in self.namemap.items():
                    newInData[input_name] = inData[output_name]
            newInData[ppfrunscript.INFOKEY] = dict(inData[ppfrunscript.INFOKEY])
            for actor in self.listDownStreamActor:
                actor.trigger(newInData)
        except Exception as e:
            logger.exception(e)
            raise


class EsrfWorkflow(Workflow):
    def __init__(self, esrfgraph, varinfo):
        name = repr(esrfgraph)
        super().__init__(name)

        # When triggering a task, the output dict of the previous task
        # is merged with the input dict of the current task.
        self.startargs = {
            ppfrunscript.INFOKEY: {"varinfo": varinfo, "enable_logging": False}
        }
        self.graph_to_actors(esrfgraph, varinfo)

    def _clean_workflow(self):
        # task_name -> EsrfPythonActor
        self._taskactors = dict()
        self.listActorRef = list()  # values of taskactors

        # source_name -> condition_name -> DecodeRouterActor
        self._routeractors = dict()

        # source_name -> target_name -> (NameMapperActor, bool)
        self._sourceactors = dict()

        # target_name -> EsrfPythonActor or InputMergeActor
        self._targetactors = dict()

        self._start_actor = StartActor(parent=self, name="Start")
        self._stop_actor = StopActor(parent=self, name="Stop")
        self._join_stop_actor = InputMergeActor(parent=self, name="Join before stop")
        self._connect_actors(self._join_stop_actor, self._stop_actor)

        self._error_actor = ErrorHandler(parent=self, name="Stop on error")
        self._connect_actors(self._error_actor, self._stop_actor)

    def graph_to_actors(self, taskgraph, varinfo):
        self._clean_workflow()
        self._create_task_actors(taskgraph)
        self._create_router_actors(taskgraph)
        self._compile_source_actors(taskgraph)
        self._compile_target_actors(taskgraph)
        self._connect_start_actor(taskgraph)
        self._connect_sources_to_targets()

    def _connect_actors(
        self, source_actor, target_actor, required=True, on_error=False, **kw
    ):
        on_error |= isinstance(target_actor, ErrorHandler)
        if on_error:
            source_actor.connectOnError(target_actor, **kw)
            logger.info(
                f"Connect (on error): '{source_actor.name}' -> '{target_actor.name}'"
            )
        else:
            source_actor.connect(target_actor, **kw)
            logger.info(f"Connect: '{source_actor.name}' -> '{target_actor.name}'")
        if required and isinstance(target_actor, JoinActor):
            target_actor.increaseNumberOfThreads()
            logger.info(f" {target_actor.name}: {target_actor.numberOfThreads} threads")

    def _create_task_actors(self, taskgraph):
        # task_name -> EsrfPythonActor
        taskactors = self._taskactors
        error_actor = self._error_actor
        imported = set()
        for node_name, node_attrs in taskgraph.graph.nodes.items():
            # Pre-import to speedup execution
            name, importfunc = task_executable(node_attrs, node_name=node_name)
            if name not in imported:
                imported.add(name)
                if importfunc:
                    importfunc(name)

            actor = EsrfPythonActor(
                node_name,
                node_attrs,
                script=ppfrunscript.__name__ + ".dummy",
                parent=self,
                errorHandler=None,
            )
            if not taskgraph.has_successors(node_name, on_error=True):
                self._connect_actors(actor, error_actor)
            taskactors[node_name] = actor
            self.addActorRef(actor)

    def _create_router_actors(self, taskgraph):
        """Insert router actors (one per target and output name) behind
        actors with conditional links. Actors without unconditional links
        are connected to the stop actor.
        """
        # source_name -> condition_name -> DecodeRouterActor
        routeractors = self._routeractors
        # task_name -> EsrfPythonActor
        taskactors = self._taskactors
        for source_name in taskgraph.graph.nodes:
            routers = routeractors[source_name] = dict()
            source_actor = taskactors[source_name]
            source_attrs = taskgraph.graph.nodes[source_name]
            is_ppfmethod = bool(source_attrs.get("ppfmethod")) or bool(
                source_attrs.get("ppfport")
            )
            for target_name in taskgraph.successors(source_name):
                link_attrs = taskgraph.graph[source_name][target_name]
                conditions = link_attrs.get("conditions", dict())
                for outname, outvalue in conditions.items():
                    router = routers.get(outname)
                    if router is None:
                        router = self._create_router_actor(
                            source_actor,
                            source_name,
                            outname,
                            is_ppfmethod=is_ppfmethod,
                        )
                        routers[outname] = router
                    if outvalue not in router.listPort:
                        router.listPort.append(outvalue)
            # TODO: "end node" is not fully determined yet
            if not taskgraph.is_end_node(source_name):
                continue
            if routers:
                for router_actor in routers.values():
                    if DecodeRouterActor.OTHERVALUE in router_actor.listPort:
                        continue
                    router_actor.listPort.append(DecodeRouterActor.OTHERVALUE)
                    self._connect_actors(
                        router_actor,
                        self._join_stop_actor,
                        expectedValue=DecodeRouterActor.OTHERVALUE,
                        required=False,  # TODO: sometimes True
                    )
            else:
                required = not taskgraph.in_optional_branch(source_name)
                self._connect_actors(
                    source_actor, self._join_stop_actor, required=required
                )

    def _create_router_actor(self, source_actor, source_name, outname, is_ppfmethod):
        routername = f"Output condition {repr(outname)} of {repr(source_name)}"
        router = DecodeRouterActor(
            self,
            name=routername,
            itemName=outname,
            listPort=[],
            errorHandler=None,
            is_ppfmethod=is_ppfmethod,
        )
        self._connect_actors(source_actor, router)
        return router

    def _compile_source_actors(self, taskgraph):
        """The source actor for the next task is either

        * the task actor itself: EsrfPythonActor
        * a router actor when the link is condition: DecodeRouterActor
        * a join actor of multiple router actors: JoinActor
        """
        # source_name -> target_name -> [(NameMapperActor, bool)]
        sourceactors = self._sourceactors

        for source_name in taskgraph.graph.nodes:
            sourceactors[source_name] = dict()
            for target_name in taskgraph.graph.successors(source_name):
                lst = []
                actor, required = self._create_source_actor(
                    taskgraph, source_name, target_name
                )
                if actor is not None:
                    lst.append((actor, required))
                actor, required = self._create_source_on_error_actor(
                    taskgraph, source_name, target_name
                )
                if actor is not None:
                    lst.append((actor, required))
                sourceactors[source_name][target_name] = lst

    def _create_source_actor(self, taskgraph, source_name, target_name):
        # task_name -> EsrfPythonActor
        taskactors = self._taskactors
        # source_name -> condition_name -> DecodeRouterActor
        routeractors = self._routeractors

        link_attrs = taskgraph.graph[source_name][target_name]
        conditions = link_attrs.get("conditions", dict())
        on_error = link_attrs.get("on_error", False)

        # One router actor for each output name
        routers = dict()
        for outname in conditions:
            routers[outname] = routeractors[source_name][outname]

        # Merge into one single source actor
        connectkw = dict()
        nrouters = len(routers)
        if nrouters == 0:
            if on_error:
                return None, None
            # ESRFTaskActor
            source_actor = taskactors[source_name]
        elif nrouters == 1:
            # DecodeRouterActor
            for outname, router_actor in routers.items():
                value = conditions[outname]
                source_actor = router_actor
                connectkw["expectedValue"] = value
        else:
            # JoinActor
            name = f"Join routers {source_name} -> {target_name}"
            source_actor = JoinActor(parent=self, name=name)
            for outname, router_actor in routers.items():
                value = conditions[outname]
                self._connect_actors(router_actor, source_actor, expectedValue=value)
        # The final actor of this link does the name mapping
        final_source = self._create_name_mapper(taskgraph, source_name, target_name)
        self._connect_actors(source_actor, final_source, **connectkw)
        required = nrouters == 0
        return final_source, required

    def _create_source_on_error_actor(self, taskgraph, source_name, target_name):
        # task_name -> EsrfPythonActor
        taskactors = self._taskactors

        link_attrs = taskgraph.graph[source_name][target_name]
        if link_attrs.get("on_error", False):
            # ESRFTaskActor
            source_actor = taskactors[source_name]
            # NameMapperActor
            final_source = self._create_name_mapper(
                taskgraph, source_name, target_name, trigger_on_error=True
            )
            self._connect_actors(source_actor, final_source, on_error=True)
            return final_source, None
        else:
            return None, None

    def _create_name_mapper(
        self, taskgraph, source_name, target_name, trigger_on_error=False
    ):
        link_attrs = taskgraph.graph[source_name][target_name]
        mapall = link_attrs.get("all_arguments", dict())
        arguments = link_attrs.get("arguments", dict())
        name = f"Name mapper {source_name} -(error={trigger_on_error})-> {target_name}"
        return NameMapperActor(
            parent=self,
            name=name,
            namemap=dict(arguments),
            mapall=mapall,
            trigger_on_error=trigger_on_error,
        )

    def _compile_target_actors(self, taskgraph):
        """Insert a join actor before every task with more than
        one predecessor.
        """
        # target_name -> EsrfPythonActor or InputMergeActor
        targetactors = self._targetactors
        # task_name -> EsrfPythonActor
        taskactors = self._taskactors
        for target_name in taskgraph.graph.nodes:
            predecessors = list(taskgraph.predecessors(target_name))
            npredecessors = len(predecessors)
            if npredecessors == 0:
                targetactor = None
            elif npredecessors == 1:
                # EsrfPythonActor
                targetactor = taskactors[target_name]
            else:
                # InputMergeActor
                targetactor = InputMergeActor(
                    parent=self, name=f"Input merger of {target_name}"
                )
                self._connect_actors(targetactor, taskactors[target_name])
            targetactors[target_name] = targetactor

    def _connect_sources_to_targets(self):
        # source_name -> target_name -> (NameMapperActor, bool)
        sourceactors = self._sourceactors
        # target_name -> EsrfPythonActor or InputMergeActor
        targetactors = self._targetactors

        for source_name, sources in sourceactors.items():
            for target_name, lst in sources.items():
                for (source_actor, required) in lst:
                    target_actor = targetactors[target_name]
                    self._connect_actors(source_actor, target_actor, required=required)

    def _connect_start_actor(self, taskgraph):
        # task_name -> EsrfPythonActor
        taskactors = self._taskactors
        # target_name -> EsrfPythonActor or InputMergeActor
        targetactors = self._targetactors
        start_actor = self._start_actor
        for target_name, task_actor in taskactors.items():
            if taskgraph.is_start_node(target_name):
                target_actor = targetactors.get(target_name)
                if target_actor is None:
                    target_actor = task_actor
                self._connect_actors(start_actor, target_actor)

    def run(self, raise_on_error=True):
        self._start_actor.trigger(self.startargs)
        self._stop_actor.join(timeout=3600 * 24 * 265)
        result = self._stop_actor.outData
        ex = result.get("WorkflowException")
        if ex is None or not raise_on_error:
            return result
        else:
            info = result.get(ppfrunscript.INFOKEY, dict())
            print("\n".join(ex["traceBack"]), file=sys.stderr)
            node_name = info.get("node_name")
            err_msg = f"Task {node_name} failed"
            if ex["errorMessage"]:
                err_msg += " ({})".format(ex["errorMessage"])
            raise RuntimeError(err_msg)


def job(graph, representation=None, varinfo=None, raise_on_error=True):
    esrfgraph = load_graph(source=graph, representation=representation)
    ppfgraph = EsrfWorkflow(esrfgraph, varinfo)
    return ppfgraph.run(raise_on_error=raise_on_error)
