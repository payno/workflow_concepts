#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


function install_project()
{
    local projdir=${SCRIPT_ROOT}/$1
    python -m pip install -r $projdir/requirements.txt -r $projdir/requirements-dev.txt
    python -m pip install -e $projdir
}


function install_pypushflow()
{
    # Do not use the fork on PyPi
    python -m pip install git+ssh://git@gitlab.esrf.fr/workflow/pypushflow.git@main
}


function main()
{
    python -m pip install --upgrade pip setuptools wheel

    install_pypushflow

    # Core projects for the workflow eco system
    install_project esrftaskgraph
    install_project esrf2multiprocessing
    install_project esrf2luigi
    install_project esrf2pypushflow
    install_project esrf2orange3
    install_project esrf2paradag
    install_project esrf2dask

    # Task and graph libraries
    install_project tasklib
    install_project taskgraphlib
    install_project orange3widgetlib

    python -m pip install ipykernel
    python -m ipykernel install --user --name workflow_concepts
}

main
